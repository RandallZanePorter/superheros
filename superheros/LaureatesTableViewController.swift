//
//  LaureatesTableViewController.swift
//  superheros
//
//  Created by Randall Zane Porter on 4/12/19.
//  Copyright © 2019 Randall Zane Porter. All rights reserved.
//

import UIKit

class LaureatesTableViewController: UITableViewController {

    var laureates:[Laurate] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        LaureateFetcher.init().fetchLaureates()
        NotificationCenter.default.addObserver(self, selector:#selector(laureatesFound(notification:)),name: NSNotification.Name(rawValue:"found the laureates"), object:nil)
    }
    
    @objc func laureatesFound(notification:Notification){
        laureates = notification.object as! [Laurate]
        tableView.reloadData()
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return laureates.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:CustomTableViewCell = tableView.dequeueReusableCell(withIdentifier: "laureate")! as! CustomTableViewCell
        let laureate:Laurate = laureates[indexPath.row]
        cell.customCellTitle.text = laureate.firstname + " " + laureate.surname
        cell.customCellSubtitle.text = laureate.born + " - " + laureate.died
        return cell
    }
}
