//
//  LaureateFetcher.swift
//  superheros
//
//  Created by Randall Zane Porter on 4/12/19.
//  Copyright © 2019 Randall Zane Porter. All rights reserved.
//

import Foundation

class LaureateFetcher {
    let dataURL:String = "https://www.dropbox.com/s/7dhdrygnd4khgj2/laureates.json?dl=1"
    
    func fetchLaureates() -> Void {
        print(dataURL)
        let urlSession = URLSession.shared
        let url = URL(string: dataURL)
        urlSession.dataTask(with: url!, completionHandler: displayLaureates).resume()
    }
    
    func displayLaureates(data:Data?, urlResponse:URLResponse?, error:Error?)->Void {
        do {
            var squad:[Laurate] = []
            let rawSquad = try JSONSerialization.jsonObject(with: data!, options: .allowFragments)  as?  [[String:Any]]
            
            for i in 0..<rawSquad!.count {
                let laur:[String:Any] = rawSquad![i]
                let firstname:String = laur["firstname"] == nil ? "" : laur["firstname"] as! String
                let surname:String = laur["surname"] == nil ? "" : laur["surname"] as! String
                let born:String = laur["born"] == nil ? "" : laur["born"] as! String
                let died:String = laur["died"] == nil ? "" : laur["died"] as! String
                squad.append(Laurate(firstname: firstname, surname: surname, born: born, died: died))
                if i % 10 == 0 { //took way too long without this
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue:"found the laureates"), object: squad)
                }
            }
            
            print("Found \(squad.count) laureates.")
            NotificationCenter.default.post(name: NSNotification.Name(rawValue:"found the laureates"), object: squad)
        } catch {
            print("Error: \(error)")
        }
        
    }
}
