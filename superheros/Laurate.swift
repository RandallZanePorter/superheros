//
//  Laureate.swift
//  superheros
//
//  Created by Randall Zane Porter on 4/12/19.
//  Copyright © 2019 Randall Zane Porter. All rights reserved.
//

import Foundation

struct Laurate : Codable {
    //var id:String
    var firstname:String
    var surname:String
    var born:String
    var died:String
    /*var bornCountry:String
    var bornCountryCode:String
    var bornCity:String
    var diedCountry:String
    var diedCountryCode:String
    var diedCity:String
    var gender:String
    var prizes:[Prize]*/
}

struct Prize : Codable {
    var year:String
    var category:String
    var share:String
    var motivation:String
    var affiliations:[Affiliation]
}

struct Affiliation : Codable {
    var name:String
    var city:String
    var country:String
}
