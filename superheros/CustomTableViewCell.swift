//
//  CustomTableViewCell.swift
//  superheros
//
//  Created by Randall Zane Porter on 4/12/19.
//  Copyright © 2019 Randall Zane Porter. All rights reserved.
//

import UIKit

class CustomTableViewCell: UITableViewCell {

    @IBOutlet weak var customCellTitle: UILabel!
    @IBOutlet weak var customCellSubtitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
