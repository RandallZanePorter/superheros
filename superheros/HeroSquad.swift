//
//  HeroSquad.swift
//  superheros
//
//  Created by Randall Zane Porter on 4/12/19.
//  Copyright © 2019 Randall Zane Porter. All rights reserved.
//

import Foundation

struct HeroSquad: Codable {
    var squadName:String
    var homeTown:String
    var formed:Int
    var secretBase:String
    var active:Bool
    var members:[Hero]
}

struct Hero : Codable {
    var name:String
    var age:Int
    var secretIdentity:String
    var powers:[String]
    
    func getPowers() -> String {
        var powerString:String = ""
        for power in powers {
            powerString += power + ", "
        }
        powerString.remove(at: powerString.lastIndex(of: ",")!)
        return powerString
    }
}
