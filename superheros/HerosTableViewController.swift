//
//  HerosTableViewController.swift
//  superheros
//
//  Created by Randall Zane Porter on 4/12/19.
//  Copyright © 2019 Randall Zane Porter. All rights reserved.
//

import UIKit

class HerosTableViewController: UITableViewController {

    var heros:[Hero] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        SuperheroFetcher.init().fetchHeros()
        NotificationCenter.default.addObserver(self, selector:#selector(herosFound(notification:)),name: NSNotification.Name(rawValue:"found the heros"), object:nil)
    }
    
    @objc func herosFound(notification:Notification){
        let squad = notification.object as! HeroSquad
        heros = squad.members
        tableView.reloadData()
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return heros.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell:CustomTableViewCell = tableView.dequeueReusableCell(withIdentifier: "hero")! as! CustomTableViewCell
        let hero:Hero = heros[indexPath.row]
        //cell.textLabel?.text = "\(hero.name) (aka: \(hero.secretIdentity))"
        //cell.detailTextLabel?.text = hero.getPowers()
        cell.customCellTitle.text = "\(hero.name) (aka: \(hero.secretIdentity))"
        cell.customCellSubtitle.text = hero.getPowers()
        return cell
    }

}
