//
//  SuperheroFetcher.swift
//  superheros
//
//  Created by Randall Zane Porter on 4/11/19.
//  Copyright © 2019 Randall Zane Porter. All rights reserved.
//

import Foundation

class SuperheroFetcher {
    let dataURL:String = "https://www.dropbox.com/s/wpz5yu54yko6e9j/squad.json?dl=1"
    
    func fetchHeros() -> Void {
        print(dataURL)
        let urlSession = URLSession.shared
        let url = URL(string: dataURL)
        urlSession.dataTask(with: url!, completionHandler: displayHeros).resume()
    }
    
    func displayHeros(data:Data?, urlResponse:URLResponse?, error:Error?)->Void {
        do {
            let squad = try JSONDecoder().decode(HeroSquad.self, from: data!)
            NotificationCenter.default.post(name: NSNotification.Name(rawValue:"found the heros"), object: squad)
            print("Found \(squad.members.count) heros.")
        } catch {
            print(error)
        }
        
    }
}
